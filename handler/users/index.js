const create = require('./create');
const getAll = require('./getAll');
const get = require('./get');
const update = require('./update');
const deleteUser = require('./deleteUser');

module.exports = {
    create,
    getAll,
    get,
    update,
    deleteUser
};