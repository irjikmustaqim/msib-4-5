const sql = require('../../db_config/db')
module.exports = (req, res) => {
    

    sql.execute("SELECT * FROM user_game;",[],function(err, results, fields) {
        console.log(results); // results contains rows returned by server
        console.log(fields); // fields contains extra meta data about results, if available
        
        if(err){
            res.status(404).json({
                status: 'error',
                message: err,
                data: {}
            })
            return
        }
        if(results.length == 0){
            res.status(404).json({
                    status: 'error',
                    message: "The data is not exist",
                    data: {}
                });
                return
            }
            
        

        res.status(200).json({
            status: 'success',
            data: results
        });
      })
};