const fs = require('fs');
const sql = require('../../db_config/db')

module.exports = (req, res) => {
    let user_id = req.params.id;

    sql.execute("DELETE FROM user_game WHERE id=?;",[user_id],function(err, results, fields) {
        console.log(results); // results contains rows returned by server
        console.log(fields); // fields contains extra meta data about results, if available
        
        if(err){
            res.status(404).json({
                status: 'error',
                message: err,
                data: {}
            });
            return
        }

        res.status(200).json({
            status: 'success',
            data: results
        });
      })
   
};