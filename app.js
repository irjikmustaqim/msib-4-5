const express = require('express');
const app = express();

const port = 3000;

app.use(express.json());
app.use(express.static("docs"))



const indexRouter = require('./routes/index');
const userRouter = require('./routes/users');

app.use('/', indexRouter);
app.use('/users', userRouter);

app.listen(port, () => {
    console.log(`Aplikasi berjalan pada port ${port}`);
});