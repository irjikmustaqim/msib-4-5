const express = require('express');
const router = express.Router();
const userHandler = require('../handler/users');

router.post('/', userHandler.create);
router.get('/', userHandler.getAll);
router.get('/:id', userHandler.get);
router.put('/:id', userHandler.update);
router.delete('/:id', userHandler.deleteUser);

module.exports = router;


