const express = require('express');
const router = express.Router();
const path = require("path")


router.get('/', (req, res) => {
    res.status(200).json({
        status: "success",
        message: "welcome to my first app",
        data: {}
    });
});

router.get('/api-docs', (req, res) => {
    res.sendFile(path.dirname(__dirname)+"/docs/index.html")
    
});

module.exports = router;