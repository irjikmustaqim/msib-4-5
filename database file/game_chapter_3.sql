-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2022 at 03:50 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `game_chapter_3`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_game`
--

CREATE TABLE `user_game` (
  `id` int(11) NOT NULL,
  `username` varchar(1000) NOT NULL,
  `password` varchar(1000) NOT NULL,
  `umur` int(11) NOT NULL,
  `jenis_kelamin` char(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_game`
--

INSERT INTO `user_game` (`id`, `username`, `password`, `umur`, `jenis_kelamin`) VALUES
(4, 'ujang16', '12345678', 23, 'Laki - Laki'),
(5, 'ucup', 'hello', 23, 'Laki - laki'),
(6, 'ucup', 'hello', 23, 'Laki - laki'),
(7, 'ucup', 'hello', 23, 'Laki - laki'),
(8, 'ucup', 'hello', 23, 'Laki - laki');

-- --------------------------------------------------------

--
-- Table structure for table `user_histori`
--

CREATE TABLE `user_histori` (
  `id_histori` int(11) NOT NULL,
  `tanggal_bermain` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `skor` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_game`
--
ALTER TABLE `user_game`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_histori`
--
ALTER TABLE `user_histori`
  ADD PRIMARY KEY (`id_histori`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_game`
--
ALTER TABLE `user_game`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_histori`
--
ALTER TABLE `user_histori`
  MODIFY `id_histori` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
